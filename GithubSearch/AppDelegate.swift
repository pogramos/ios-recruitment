//
//  AppDelegate.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 29/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var appCoordinator: AppCoordinator!


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    Theme.setup()
    window = UIWindow(frame: UIScreen.main.bounds)
    appCoordinator = AppCoordinator(in: window!)
    appCoordinator.start()
    return true
  }

}

