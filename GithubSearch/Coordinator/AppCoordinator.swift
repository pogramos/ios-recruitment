//
//  AppCoordinator.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 31/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

final class AppCoordinator: RootCoordinator {
  var childCoordinators: [Coordinator] = []

  var rootViewController: UIViewController {
    return navigationController
  }

  private lazy var navigationController: UINavigationController = self.makeNavigationController()
  private func makeNavigationController() -> UINavigationController {
    let navigationController = UINavigationController()
    // config
    return navigationController
  }

  let window: UIWindow

  init(in window: UIWindow) {
    self.window = window

    self.window.rootViewController = self.rootViewController
    self.window.makeKeyAndVisible()
  }

  func start() {
    showUserSearchController()
  }

  // MARK: Navigation

  private func showUserSearchController() {
    let userSearchController = UserSearchViewController(coordinatedBy: self)
    navigationController.viewControllers = [userSearchController]
  }
}

extension AppCoordinator: UserFlowCoordinatorDelegate {
  func showUserController(with user: User, repositories: [Repository]) {
    let userController = UserViewController(for: user, repositories: repositories)
    navigationController.pushViewController(userController, animated: true)
  }
}
