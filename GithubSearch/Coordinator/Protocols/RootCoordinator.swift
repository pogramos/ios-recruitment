//
//  RootCoordinator.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 31/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

protocol RootCoordinatorProvider: class {
  var rootViewController: UIViewController { get }
}

typealias RootCoordinator = Coordinator & RootCoordinatorProvider
