//
//  Coordinator.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation


protocol Coordinator: class {
  var childCoordinators: [Coordinator] { get set }
}

extension Coordinator {
  func add(childCoordinator: Coordinator) {
    childCoordinators.append(childCoordinator)
  }

  func remove(childCoordinator: Coordinator) {
    childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
  }
}
