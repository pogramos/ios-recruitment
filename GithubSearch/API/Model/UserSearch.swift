//
//  User.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

struct UserSearch: Codable, Equatable {
    let login: String?
    let id: Int?
    let nodeID, avatarURL, gravatarID, url: String?
    let htmlURL, followersURL, subscriptionsURL, organizationsURL: String?
    let reposURL, receivedEventsURL, type: String?
    let score: Double?

    enum CodingKeys: String, CodingKey {
        case login, id
        case nodeID = "node_id"
        case avatarURL = "avatar_url"
        case gravatarID = "gravatar_id"
        case url
        case htmlURL = "html_url"
        case followersURL = "followers_url"
        case subscriptionsURL = "subscriptions_url"
        case organizationsURL = "organizations_url"
        case reposURL = "repos_url"
        case receivedEventsURL = "received_events_url"
        case type, score
    }
}

extension UserSearch {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(UserSearch.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
