//
//  User.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

struct User: Codable {
  let login: String?
  let id: Int
  let avatarURL: String
  let nodeID, gravatarID, url: String?
  let htmlURL, followersURL, followingURL, gistsURL: String?
  let starredURL, subscriptionsURL, organizationsURL, reposURL: String?
  let eventsURL, receivedEventsURL, type: String?
  let siteAdmin: Bool?
  let name, company, blog, location: String?
  let email: String?
  let hireable: Bool?
  let bio: String?
  let publicRepos, publicGists, followers, following: Int?
  let createdAt, updatedAt: String?

  enum CodingKeys: String, CodingKey {
    case login, id
    case nodeID = "node_id"
    case avatarURL = "avatar_url"
    case gravatarID = "gravatar_id"
    case url
    case htmlURL = "html_url"
    case followersURL = "followers_url"
    case followingURL = "following_url"
    case gistsURL = "gists_url"
    case starredURL = "starred_url"
    case subscriptionsURL = "subscriptions_url"
    case organizationsURL = "organizations_url"
    case reposURL = "repos_url"
    case eventsURL = "events_url"
    case receivedEventsURL = "received_events_url"
    case type
    case siteAdmin = "site_admin"
    case name, company, blog, location, email, hireable, bio
    case publicRepos = "public_repos"
    case publicGists = "public_gists"
    case followers, following
    case createdAt = "created_at"
    case updatedAt = "updated_at"
  }
}

// MARK: Convenience initializers

extension User {
  init?(data: Data) {
    guard let me = try? JSONDecoder().decode(User.self, from: data) else { return nil }
    self = me
  }

  init?(_ json: String, using encoding: String.Encoding = .utf8) {
    guard let data = json.data(using: encoding) else { return nil }
    self.init(data: data)
  }

  init?(fromURL url: String) {
    guard let url = URL(string: url) else { return nil }
    guard let data = try? Data(contentsOf: url) else { return nil }
    self.init(data: data)
  }

  var jsonData: Data? {
    return try? JSONEncoder().encode(self)
  }

  var json: String? {
    guard let data = self.jsonData else { return nil }
    return String(data: data, encoding: .utf8)
  }
}
