//
//  Asset.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Moya

enum Asset {
  case getImage(url: String)
}

extension Asset: TargetType {

  var baseURL: URL {
    switch self {
    case let .getImage(url):
      var newUrl = url.split(separator: "/").map { String($0) }
      newUrl.removeLast()
      guard let newUrlObject = URL(string: newUrl.joined(separator: "/")) else {
        fatalError("Invalid url format")
      }
      return newUrlObject
    }
  }

  var path: String {
    switch self {
    case let .getImage(url):
      let newUrl = url.split(separator: "/").map { String($0) }
      return newUrl.last ?? ""
    }
  }

  var method: Method {
    switch self {
    case .getImage:
      return .get
    }
  }

  var sampleData: Data {
    switch self {
    case .getImage:
      return Data()
    }
  }

  var task: Task {
    switch self {
    case .getImage:
      return .requestPlain
    }
  }

  var headers: [String : String]? {
    return ["Content-type": "image/png"]
  }
}
