//
//  Github.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Moya

enum Github {
  case searchUser(name: String)
  case getUserData(name: String)
  case getRepository(name: String)
}


extension Github: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.github.com") else {
            fatalError()
        }
        return url
    }

    var path: String {
        switch self {
        case .searchUser(_):
            return "/search/users"
        case let .getUserData(name):
          return "/users/\(name)"
        case let .getRepository(name):
          return "/users/\(name)/repos"
        }
    }

    var method: Method {
        switch self {
        case .searchUser, .getUserData, .getRepository:
            return .get
        }
    }

    var sampleData: Data {
        switch self {
        case .searchUser:
          return Data()
        case .getUserData:
          guard let url = Bundle.main.url(forResource: "user", withExtension: ".json"),
            let data = try? Data(contentsOf: url) else {
              return Data()
          }
          return data
        case .getRepository:
          guard let url = Bundle.main.url(forResource: "repository", withExtension: ".json"),
            let data = try? Data(contentsOf: url) else {
              return Data()
          }
          return data
        }
    }

    var task: Task {
        switch self {
        case let .searchUser(name):
          return .requestParameters(parameters: ["q": name], encoding: URLEncoding.queryString)
        case .getUserData, .getRepository:
          return .requestPlain
        }
    }

    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
