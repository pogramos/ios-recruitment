//
//  UIColor+Extension.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(_ hex: String, alpha: CGFloat = 1) {
    assert(!hex.hasPrefix("#") && !hex.hasPrefix("0x"), "Sua string hexadecimal deve ter somente 6 números")
    let scanner = Scanner(string: hex)
    var hexValue: UInt64 = 0
    scanner.scanHexInt64(&hexValue)

    let red = CGFloat((hexValue & 0xFF0000) >> 16) / 255
    let green = CGFloat((hexValue & 0x00FF00) >> 8) / 255
    let blue = CGFloat((hexValue & 0x0000FF)) / 255

    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
