//
//  String+Extension.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

extension String {
  var escaped: String {
    guard let string = addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
      return ""
    }
    return string
  }

  var utf8Encoded: Data {
    guard let data = data(using: .utf8) else {
      return Data()
    }
    return data
  }
}
