//
//  AnyObject+Extension.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

protocol TypeName: AnyObject {
  var typeName: String { get }
}

extension TypeName {
  static var typeName: String {
    return String(describing: self)
  }

  var typeName: String {
    return String(describing: type(of: self))
  }
}

extension NSObject {
  static var typeName: String {
    return String(describing: self)
  }
  
  var typeName: String {
    return String(describing: type(of: self))
  }
}
