//
//  UIView+Extension.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

extension UIView {
  class func nib() -> UINib {
    return UINib(nibName: typeName, bundle: nil)
  }
}
