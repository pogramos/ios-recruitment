//
//  Theme.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

final class Theme {
  class func setup() {
    setupNavigationBarStyle()
  }

  class private func setupNavigationBarStyle() {
    let appearance = UINavigationBar.appearance()
    appearance.tintColor = UIColor("6B9EEF")
    appearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
  }
}
