//
//  GCDBlackBox.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(_ block: @escaping () -> Void) {
  DispatchQueue.main.async(execute: block)
}
