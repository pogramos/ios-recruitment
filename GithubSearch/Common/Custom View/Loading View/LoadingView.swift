//
//  LoadingView.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoadingView: BaseCustomView {
  private let disposeBag = DisposeBag()
  private let kDefaultTimeInterval: TimeInterval = 0.3
  @IBOutlet weak var blurEffectView: UIVisualEffectView!
  @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

  var isLoading = PublishSubject<Bool>()


  override init(frame: CGRect) {
    super.init(frame: frame)
    isLoading
      .asObservable()
      .subscribe(onNext: { [weak self] (loading) in
        self?.isHidden = !loading
        if loading {
          self?.activityIndicatorView.startAnimating()
        } else {
          self?.activityIndicatorView.stopAnimating()
        }
      })
      .disposed(by: disposeBag)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func show(on parentView: UIView?) {
    contentView.alpha = 0

    parentView?.addSubview(self)

    UIView.animate(withDuration: kDefaultTimeInterval) {
      self.contentView.alpha = 1
    }
  }

  func hide() {
    UIView.animate(withDuration: kDefaultTimeInterval, animations: {
      self.contentView.alpha = 0
    }, completion: { finished in
      if finished {
        self.contentView.removeFromSuperview()
      }
    })
  }

}
