//
//  GradientView.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

@IBDesignable
final class GradientView: UIView {
  @IBInspectable
  var firstColor: UIColor = .white {
    didSet {
      loadGradient()
    }
  }

  @IBInspectable
  var secondColor: UIColor = .white {
    didSet {
      loadGradient()
    }
  }

  @IBInspectable
  var thirdColor: UIColor = .white {
    didSet {
      loadGradient()
    }
  }

  @IBInspectable
  var startingPoint: CGPoint = .zero {
    didSet {
      loadGradient()
    }
  }

  @IBInspectable
  var endingPoint: CGPoint = CGPoint(x: 0, y: 1) {
    didSet {
      loadGradient()
    }
  }

  private func loadGradient() {
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
    gradientLayer.startPoint = startingPoint
    gradientLayer.endPoint = endingPoint
    gradientLayer.frame = bounds
    if let top = layer.sublayers?.first, top is CAGradientLayer {
      top.removeFromSuperlayer()
    }
    layer.addSublayer(gradientLayer)
  }
}
