//
//  EmptyStateUsersView.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit

class EmptyStateUsersView: BaseCustomView {
  @IBOutlet weak var emptyStateText: UILabel!
}
