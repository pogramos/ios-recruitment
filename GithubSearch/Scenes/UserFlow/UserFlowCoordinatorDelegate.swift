//
//  UserFlowCoordinatorDelegate.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation

protocol UserFlowCoordinatorDelegate: class {
  func showUserController(with user: User, repositories: [Repository])
}
