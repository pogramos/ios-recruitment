//
//  UserSearchViewController.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserSearchViewController: UIViewController {
  var viewModel: UserSearchViewModel!
  private var loadingView: LoadingView!
  private let emptyStateView = EmptyStateUsersView()
  private let disposeBag = DisposeBag()
  private let searchController = UISearchController(searchResultsController: nil)

  @IBOutlet weak var tableView: UITableView!

  init(coordinatedBy coordinator: UserFlowCoordinatorDelegate) {
    super.init(nibName: nil, bundle: nil)
    viewModel = UserSearchViewModel()
    viewModel.coordinator = coordinator
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Busca"
    configSearchController()
    configTableView()
    loadingView = LoadingView(frame: view.bounds)
    view.addSubview(loadingView)
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBar.prefersLargeTitles = true
    loadingView.isLoading.onNext(false)
  }

  private func configTableView() {
    tableView?.register(UserFlowTableViewCell.nib(), forCellReuseIdentifier: UserFlowTableViewCell.typeName)
    tableView?.backgroundView = emptyStateView
  }

  private func configSearchController() {
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Procurar usuários"
    navigationItem.searchController = searchController
    navigationItem.hidesSearchBarWhenScrolling = false
    definesPresentationContext = true
    searchController.isActive = true
    searchController.searchBar.becomeFirstResponder()
  }

  private func bind() {
    viewModel
      .users
      .do(onNext: { [weak self] (users) in
        self?.emptyStateView.isHidden = !users.isEmpty
        self?.tableView.separatorStyle = users.isEmpty ? .none : .singleLine
      })
      .bind(to: tableView.rx.items(cellIdentifier: UserFlowTableViewCell.typeName,
                                   cellType: UserFlowTableViewCell.self)) { (_, user, cell) in
        cell.selectionStyle = .none
        cell.textLabel?.text = user.login
        cell.detailTextLabel?.text = user.url
      }
      .disposed(by: disposeBag)

    viewModel.bindFetchUsers(searchController.searchBar.rx.text.orEmpty)

    tableView
      .rx
      .itemSelected
      .flatMap(self.viewModel.fetchUserData)
      .subscribe(onNext: { [weak self] user in
        self?.viewModel.navigate(with: user)
      })
      .disposed(by: disposeBag)

    viewModel
      .isLoading
      .subscribe(onNext: { (isLoading) in
        self.loadingView.isLoading.onNext(isLoading)
      })
      .disposed(by: disposeBag)
  }

}
