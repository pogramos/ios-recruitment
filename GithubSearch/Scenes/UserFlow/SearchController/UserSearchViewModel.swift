//
//  UserSearchViewModel.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 30/10/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMoya
import Moya

struct UserSearchViewModel {
  private let provider = MoyaProvider<Github>()
  private let kDefaultThrottleTime = 0.5
  private let kKeyPath = "items"

  weak var coordinator: UserFlowCoordinatorDelegate?

  var users: Observable<[UserSearch]> {
    return usersVariable.asObservable()
  }

  var isLoading: Observable<Bool> {
    return isLoadingVariable.asObservable()
  }

  var repositories: Observable<[Repository]> {
    return repositoryVariable.asObservable()
  }

  private let disposeBag = DisposeBag()

  private let usersVariable = BehaviorRelay<[UserSearch]>(value: [])
  private let isLoadingVariable = BehaviorRelay<Bool>(value: false)
  private let repositoryVariable = BehaviorRelay<[Repository]>(value: [])

  func navigate(with user: User) {
    BehaviorRelay<User>(value: user)
      .asObservable()
      .do(onNext: { _ in
        self.isLoadingVariable.accept(true)
      })
      .flatMap(fetchUserRepository)
      .do(onNext: { _ in
        self.isLoadingVariable.accept(false)
      })
      .subscribe(onNext: { (repositories) in
        self.coordinator?.showUserController(with: user, repositories: repositories)
      })
      .disposed(by: disposeBag)
  }
}

extension UserSearchViewModel {
  func bindFetchUsers(_ observable: ControlProperty<String>) {
    observable
      .do(onNext: { (_) in
        self.isLoadingVariable.accept(true)
      })
      .debounce(kDefaultThrottleTime, scheduler: MainScheduler.instance)
      .distinctUntilChanged()
      .flatMapLatest(fetchUsers)
      .do(onNext: { (_) in
        self.isLoadingVariable.accept(false)
      })
      .observeOn(MainScheduler.instance)
      .bind(to: usersVariable)
      .disposed(by: disposeBag)
  }

  func fetchUserData(_ indexPath: IndexPath) -> Observable<User> {
    let item = usersVariable.value[indexPath.row]
    return provider
      .rx
      .request(.getUserData(name: item.login ?? ""))
      .map(User.self)
      .asObservable()
  }

  private func fetchUsers(_ query: String) -> Observable<[UserSearch]> {
    if query.isEmpty {
      return .just([UserSearch]())
    }
    return provider
      .rx
      .request(.searchUser(name: query))
      .filterSuccessfulStatusCodes()
      .map([UserSearch].self, atKeyPath: kKeyPath, using: JSONDecoder(), failsOnEmptyData: false)
      .catchErrorJustReturn([UserSearch]())
      .asObservable()
  }

  private func fetchUserRepository(_ user: User) -> Observable<[Repository]> {
    guard let login = user.login else {
      return .just([Repository]())
    }
    isLoadingVariable.accept(true)
    return provider
      .rx
      .request(.getRepository(name: login))
      .map([Repository].self)
      .catchErrorJustReturn([Repository]())
      .asObservable()
  }
}
