//
//  UserTableHeaderView.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit
import RxSwift
import RxMoya
import Moya

class UserTableHeaderView: BaseCustomView {

  private let provider = MoyaProvider<Asset>()
  private let disposeBag = DisposeBag()

  @IBOutlet weak var usersPortraitImageView: UIImageView!
  @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
  @IBOutlet weak var userDescriptionLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var followersNumberLabel: UILabel!
  @IBOutlet weak var followingNumberLabel: UILabel!

  override init(frame: CGRect) {
    super.init(frame: frame)
    configUI()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configUI()
  }

  private func configUI() {
    usersPortraitImageView.layer.cornerRadius = usersPortraitImageView.frame.width / 2
    usersPortraitImageView.layer.masksToBounds = true
    usersPortraitImageView.layer.borderColor = UIColor.black.cgColor
    usersPortraitImageView.layer.borderWidth = 2

    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [UIColor.blue.cgColor, UIColor.blue.cgColor, UIColor.blue.cgColor]
    gradientLayer.startPoint = CGPoint(x: 0, y: 0)
  }

  func config(with user: User) {
    activityIndicatorView?.startAnimating()
    activityIndicatorView?.hidesWhenStopped = true

    userDescriptionLabel?.text = user.bio
    userNameLabel?.text = user.name

    followersNumberLabel?.text = "\(user.followers ?? 0)"
    followingNumberLabel?.text = "\(user.following ?? 0)"

    if let url = URL(string: user.avatarURL), let data = try? Data(contentsOf: url) {
      usersPortraitImageView?.image = UIImage(data: data)
    }
    activityIndicatorView?.stopAnimating()
  }
}
