//
//  UserViewModel.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMoya
import Moya

struct UserViewModel {
  private let disposeBag = DisposeBag()
  private let provider = MoyaProvider<Github>()

  var user: Observable<User> {
    return userVariable.asObservable()
  }

  var repositories: Observable<[Repository]> {
    return repositoryVariable.asObservable()
  }

  var isLoading: Observable<Bool> {
    return isLoadingVariable.asObservable()
  }

  private var userVariable: BehaviorRelay<User>!
  private var repositoryVariable = BehaviorRelay<[Repository]>(value: [])
  private var isLoadingVariable = BehaviorRelay<Bool>(value: false)

  init(with user: User, repositories: [Repository]) {
    userVariable = BehaviorRelay(value: user)
    repositoryVariable.accept(repositories)
  }
}
extension UserViewModel {
  func fetchRepository() {
    user
      .flatMapLatest(fetchUserRepository)
      .do(onNext: { (_) in
        self.isLoadingVariable.accept(false)
      })
      .observeOn(MainScheduler.instance)
      .bind(to: repositoryVariable)
      .disposed(by: disposeBag)
  }

  private func fetchUserRepository(_ user: User) -> Observable<[Repository]> {
    guard let login = user.login else {
      return .just([Repository]())
    }
    isLoadingVariable.accept(true)
    return provider
      .rx
      .request(.getRepository(name: login))
      .map([Repository].self)
      .asObservable()
  }
}
