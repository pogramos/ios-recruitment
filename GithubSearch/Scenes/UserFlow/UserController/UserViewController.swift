//
//  UserViewController.swift
//  GithubSearch
//
//  Created by Guilherme Ramos on 02/11/18.
//  Copyright © 2018 Guilherme Ramos. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UserViewController: UIViewController {
  private let disposeBag = DisposeBag()
  private var tableHeaderView: UserTableHeaderView!
  private let tableBackgroundView = UIStackView()
  private var loadingView: LoadingView!
  private let emptyStateView = EmptyStateUsersView()
  @IBOutlet weak var tableView: UITableView!

  private var viewModel: UserViewModel!

  init(for user: User, repositories: [Repository]) {
    super.init(nibName: nil, bundle: nil)
    viewModel = UserViewModel(with: user, repositories: repositories)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configTableView()
    loadingView = LoadingView(frame: view.bounds)
    view.addSubview(loadingView)
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBar.prefersLargeTitles = false
    navigationController?.navigationBar.isTranslucent = true
  }

  private func configTableView() {
    tableHeaderView = UserTableHeaderView()
    tableView.tableHeaderView = tableHeaderView
    tableView?.register(UserFlowTableViewCell.nib(), forCellReuseIdentifier: UserFlowTableViewCell.typeName)
    emptyStateView.emptyStateText?.text = "Não foram econtrados repositórios para este usuário"
    tableView.backgroundView = emptyStateView
    emptyStateView.isHidden = true
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    sizeHeaderToFit()
  }

  private func sizeHeaderToFit() {
    tableHeaderView.setNeedsLayout()
    tableHeaderView.layoutIfNeeded()

    let height = tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
    tableHeaderView.frame.size.height = height

    tableView.tableHeaderView = tableHeaderView
  }

  private func bind() {
    viewModel
      .user
      .subscribe(onNext: { [weak self] (user) in
        self?.tableHeaderView.config(with: user)
        self?.tableHeaderView?.layoutIfNeeded()
      })
      .disposed(by: disposeBag)

    viewModel
      .repositories
      .do(onNext: { [weak self] repositories in
        self?.loadingView.isLoading.onNext(repositories.isEmpty)
      })
      .bind(to: tableView.rx.items(cellIdentifier: UserFlowTableViewCell.typeName,
                                   cellType: UserFlowTableViewCell.self)) { (_, repository, cell) in
        cell.textLabel?.text = repository.name
        cell.detailTextLabel?.text = repository.description
      }
      .disposed(by: disposeBag)
  }
}


